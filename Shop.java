import java.util.Scanner;
public class Shop{
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		Scanner scDouble = new Scanner(System.in);
		
		TShirts[] TShirts = new TShirts[4];
		
		for(int i = 0;i<TShirts.length;i++){
			
			System.out.println("What is the size of the T-shirt?");
			String size = sc.nextLine();
			
			
			System.out.println("What color is the T-shirt?");
			String color = sc.nextLine();
			
			System.out.println("How much is it?");
			double price = scDouble.nextDouble();
			
			TShirts[i] = new TShirts(size,color,price);
		}
		
		System.out.println(TShirts[3].getSize());
		System.out.println(TShirts[3].getColor());
		System.out.println(TShirts[3].getPrice()+"$");
		System.out.println();
		
		
		System.out.println("What color is the T-shirt?");
		TShirts[3].setColor(sc.nextLine());
		System.out.println("How much is it?");
		TShirts[3].setPrice(scDouble.nextDouble());
		
		System.out.println("This is the latest T-shirt you added:");
		System.out.println(TShirts[3].getSize());
		System.out.println(TShirts[3].getColor());
		System.out.println(TShirts[3].getPrice()+"$");
		System.out.println();
		
		TShirts[3].describe();
	}
}