public class TShirts{
	
	private String size;
	private String color;
	private double price;
	
	public void describe(){
		System.out.println("The T-shirt is "+size);
		System.out.println("The T-shirt is "+color);
		System.out.println("The T-shirt Cost "+price+"$");
	}
	
	//constructor
	
	public TShirts(String size, String color, double price){
		this.size = size;
		this.color = color;
		this.price = price;
	}
	
	//getter
	
	public String getSize(){
		return this.size;
	}
	public String getColor(){
		return this.color;
	}
	public double getPrice(){
		return this.price;
	}
	
	//setter
	
	public void setColor(String newColor) {
		this.color = newColor; 
	} 
	public void setPrice(double newPrice) {
		this.price = newPrice; 
	} 


}